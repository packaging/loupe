#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

export DEBIAN_FRONTEND=noninteractive
export CCACHE_DIR="${CI_PROJECT_DIR}/ccache"
mkdir -p "${CCACHE_DIR}"
tag="${CI_COMMIT_TAG:-0.0+0}"
version="${tag%%+*}"
patchlevel="${tag##*+}"
tmpdir="$(mktemp -d)"
nfpm="${NFPM:-2.35.3}"
architecture="$(dpkg --print-architecture)"
apt-get update
apt-get \
    -y \
    --no-install-recommends \
    install \
    apt-file \
    ca-certificates \
    cargo \
    ccache \
    cmake \
    curl \
    desktop-file-utils \
    gettext \
    git \
    itstool \
    libadwaita-1-dev \
    libgeocode-glib-dev \
    libgtkmm-4.0-dev \
    libgweather-4-dev \
    libjson-glib-dev \
    liblcms2-dev \
    libsoup-3.0-dev \
    libxml2-dev \
    meson \
    valac
apt-file update
git clone -b "${version}" https://gitlab.gnome.org/GNOME/loupe
cd loupe
meson setup build --buildtype=release --prefix /usr
meson install -C build --destdir "${tmpdir}"
if ! command -v nfpm >/dev/null 2>&1; then
    curl -L \
        "https://github.com/goreleaser/nfpm/releases/download/v${nfpm}/nfpm_${nfpm}_Linux_x86_64.tar.gz" |
        tar -xzf - -C /usr/bin nfpm
fi
ldd "${tmpdir}"/usr/bin/loupe | grep '=>' | awk '{print $3}' >"/tmp/deps.libs"
mapfile -t deps < <(apt-file --architecture "${architecture}" find -f /tmp/deps.libs | awk -F ':' '{print $1}' | sort -u | grep -v '.*-dev')
cat >"${tmpdir}/nfpm.yaml" <<EOF
name: loupe
arch: ${architecture}
version: ${tag//[a-zA-Z]/}
version_schema: none
maintainer: "Stefan Heitmüller <stefan.heitmueller@gmx.com>"
description: An image viewer application written with GTK 4, Libadwaita and Rust.
homepage: https://gitlab.gnome.org/GNOME/loupe
contents:
  - src: ${tmpdir}/usr/
    dst: /usr
    type: tree
scripts:
  preinstall: ${CI_PROJECT_DIR}/.packaging/preinstall.sh
recommends:
  - morph027-keyring
depends:
  - libgtk-4-bin
EOF
for dep in "${deps[@]}"; do
    echo "  - ${dep}" >>"${tmpdir}/nfpm.yaml"
done
nfpm package --config "${tmpdir}/nfpm.yaml" --packager deb
mkdir -p "${CI_PROJECT_DIR}/${CODENAME}"
cp ./*.deb "${CI_PROJECT_DIR}/${DISTRIB_CODENAME}"
